const moMongoClient = require('mongodb').MongoClient;
const mongodbURL = 'mongodb://jh:789999@cluster0-shard-00-00.f3kjl.mongodb.net:27017,cluster0-shard-00-01.f3kjl.mongodb.net:27017,cluster0-shard-00-02.f3kjl.mongodb.net:27017/?ssl=true&replicaSet=atlas-5u6c7o-shard-0&authSource=admin&retryWrites=true&w=majority';
const dbName = 'try-3';


module.exports = async function ()
{
    const client = await moMongoClient.connect(mongodbURL, {
        useNewUrlParser: true
    });
    console.log("connect success!");
    db = client.db(dbName);
    return db;
};

// (async () =>
// {
//     const client = new moMongoClient(mongodbURL);
//     try
//     {
//         await client.connect();
//         const database = client.db(dbName);
//         const collection = database.collection('student');
//         //插入一条数据
//         const result = await collection.insertOne({
//             name: '张三',
//             age: 18,
//             address: '湖南'
//         });
//     }
//     catch (err)
//     {
//         console.log(err);
//     }
// })();