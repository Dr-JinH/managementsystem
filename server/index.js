const mdb = require('./mdb');
const express = require('express');
const bodyParser = require('body-parser');
const ObjectId = require('mongodb').ObjectId;

var db;
const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.get('/', (req, res) =>
{
    res.send('Hello World!');
});

app.get('/provinceList', (req, res) =>
{
    res.json({
        "10101": "北京",
        "10102": "上海",
        "10103": "天津",
        "10104": "重庆",
        "10105": "黑龙江",
        "10106": "吉林",
        "10107": "辽宁",
        "10108": "内蒙古",
        "10109": "河北",
        "10110": "山西",
        "10111": "陕西",
        "10112": "山东",
        "10113": "新疆",
        "10114": "西藏",
        "10115": "青海",
        "10116": "甘肃",
        "10117": "宁夏",
        "10118": "河南",
        "10119": "江苏",
        "10120": "湖北",
        "10121": "浙江",
        "10122": "安徽",
        "10123": "福建",
        "10124": "江西",
        "10125": "湖南",
        "10126": "贵州",
        "10127": "四川",
        "10128": "广东",
        "10129": "云南",
        "10130": "广西",
        "10131": "海南",
        "10132": "香港",
        "10133": "澳门",
        "10134": "台湾"
    });
});

var student = [];

app.get('/studentList', (req, res) =>
{
    student = [];
    (async function ()
    {
        db = await mdb();
        const collection = db.collection('student');
        //所有结果
        const result = collection.find();
        while (await result.hasNext())
        {
            const doc = await result.next();
            student.push(doc);
        }
        res.json(student);
    })();
});

app.post('/addStudent', (req, res) =>
{
    (async function ()
    {
        db = await mdb();
        const collection = db.collection('student');
        //插入一条数据
        const result = await collection.insertOne(req.body);
        res.json(result);
    })();
});

app.post('/updateStudent', (req, res) =>
{
    (async function ()
    {
        db = await mdb();
        const collection = db.collection('student');
        //更新一条数据
        //去掉req.body._id
        var bodyCopy = JSON.parse(JSON.stringify(req.body));
        delete bodyCopy._id;
        const result = await collection.updateOne({
            _id: ObjectId(req.body._id)
        }, {
            $set: bodyCopy
        });
        if(result.modifiedCount > 0)
        {
            res.json({
                "status": "success",
                "message": "修改成功"
            });
        }
        else
        {
            res.json({
                "status": "error",
                "message": "修改失败"
            });
        }
    })();
});

app.post('/deleteStudent', (req, res) =>
{
    console.log(req.body);
    (async function ()
    {
        db = await mdb();
        const collection = db.collection('student');
        //删除一条或多条数据
        for (var i = 0; i < req.body.length; i++)
        {
            const result = await collection.deleteOne({
                _id: new ObjectId(req.body[i]._id)
            });
            if (result.deletedCount == 0)
            {
                console.log("delete failed!");
                res.json({
                    "status": "error",
                    "message": "删除失败"
                });
                return;
            }
        }
        res.json({
            "status": "success",
            "message": "删除成功"
        });
    }
    )();
});


app.listen(port, () =>
{
    console.log('Server is running on port 3000');
});

